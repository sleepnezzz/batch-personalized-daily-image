package com.krungsri.uchoose.batchpersonalizeddailyimage

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class BatchPersonalizedDailyImageApplication

fun main(args: Array<String>) {
    runApplication<BatchPersonalizedDailyImageApplication>(*args)
}
