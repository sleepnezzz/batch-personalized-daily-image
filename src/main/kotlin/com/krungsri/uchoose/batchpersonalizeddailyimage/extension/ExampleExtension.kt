package com.krungsri.uchoose.batchpersonalizeddailyimage.extension

import java.util.UUID

fun generateId(): String {
    return UUID.randomUUID().toString()
}