package com.krungsri.uchoose.batchpersonalizeddailyimage.extension

import org.junit.jupiter.api.Assertions.assertNotEquals
import org.junit.jupiter.api.Assertions.assertNotNull
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test

class ExampleExtensionKtTests {

    @Nested
    inner class GenerateIdTest {
        @Test
        fun shouldReturnGenerateIdIsNotNull() {
            val actual: String = generateId()
            assertNotNull(actual)
        }

        @Test
        fun shouldReturnGenerateIdIsUnique() {
            val id1: String = generateId()
            val id2: String = generateId()
            assertNotEquals(id1, id2)
        }
    }
}